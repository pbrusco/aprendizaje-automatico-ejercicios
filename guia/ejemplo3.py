#! /usr/bin/env python
# -*- coding: utf-8 -*-
import metricas as metricas

with open("y_probas.txt", "r") as file:
    y_probas = [float(l[0:-1]) for l in file.readlines()]

with open("y_actual.txt", "r") as file:
    y_actual = [l[0:-1] for l in file.readlines()]


print "y_probas primeros 5: ", y_probas[0:5]
print "y_actual primeros 5: ", y_actual[0:5]

# Ordenamos los vectores por probabilidad de menor a mayor
y_probas, y_actual = zip(*sorted(zip(y_probas, y_actual)))

# Ejercicios:
y_predicted = "Completar"

tp, tn, fp, fn = metricas.confusion_matrix(y_actual=y_actual, y_predicted=y_predicted, positive_label="perro")
# Calcular Accuracy
# Calcular TPR, FPR

# Calcular y graficar curva ROC y AUC. 



# Gráfico (necesita la librería seaborn)

# import seaborn as sns
# import matplotlib.pyplot as plt
# sns.distplot([y_probas[i] for i in range(0, len(y_actual)) if y_actual[i] == "perro"], color="blue", label="perro")
# sns.distplot([y_probas[i] for i in range(0, len(y_actual)) if y_actual[i] == "gato"], color="red", label="gato")
# plt.xlim([0,1])
# plt.legend()
# plt.tight_layout()
# plt.show()
