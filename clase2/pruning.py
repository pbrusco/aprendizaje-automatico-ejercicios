#! /usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import urllib

# URL for the Pima Indians Diabetes dataset (UCI Machine Learning Repository)
url = "http://goo.gl/j0Rvxq"# download the file
raw_data = urllib.urlopen(url)

# load the CSV file as a numpy matrix
dataset = np.loadtxt(raw_data, delimiter=",")
print(dataset.shape)

# separate the data from the target attributes
X = dataset[:,0:8] #primeras 8
y = dataset[:,8] #objetivo

# correr un árbol con parámetros default
from sklearn import tree
clf1 = tree.DecisionTreeClassifier()
clf1 = clf1.fit(X,y)
print "árbol 1 = tree.DecisionTreeClassifier()"
print "Nodos del árbol 1: ", clf1.tree_.node_count
print "Score árbol 1: ", clf1.score(X,y)

for nodes in range(10,150):
    clf = tree.DecisionTreeClassifier(max_leaf_nodes=nodes)
    clf = clf.fit(X,y)
    print "nodes_max: ", nodes, "nodes: ", clf.tree_.node_count, "score: ", clf.score(X,y)
