# usr/bin/python
# -*- coding: utf-8 -*-

import sys
import numpy as np
import cv2
import glob

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

if __name__ == '__main__':

	images_folder = sys.argv[1]
	img_filenames = glob.glob(images_folder + "/*.jpg")
	img_filenames = sorted(img_filenames, key= lambda name: int(name[name.index('.')+1:name.rindex('.')]))

	d = {}

	# outfolder = sys.argv[2]

	for filename in img_filenames:
		# if len(d.keys()) > 40:
		# 	break

		print filename
		img = cv2.imread(filename)

		h,w,t = img.shape
		# if h==374 and w==500:
		# 	cv2.imwrite(outfolder+(filename[filename.rindex("/")+1:]), img)

		if img.shape in d.keys():
			d[ img.shape ] += 1
		else:
			d[ img.shape ] = 1

	# sys.exit()

	xs = [t[0] for t in d.keys()]
	ys = [t[1] for t in d.keys()]
	z  = [d[k] for k in d.keys()]

	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	ax.bar(xs, z, zs=ys, zdir='y')

	ax.set_title('Distribucion imagenes por alto y ancho')
	ax.set_xlabel('alto')
	ax.set_ylabel('ancho')
	ax.set_zlabel('cantidad')

	plt.savefig('dataset.png')
	# plt.show()

	with open('dataset.details.txt','w') as fout:

		fout.write("alto:\n")
		fout.write("mean:"+str(np.mean(xs))+"\n")
		fout.write("max:"+str(max(xs))+"\n")
		fout.write("min:"+str(min(xs))+"\n")
		fout.write("\n")
		fout.write("ancho:\n")
		fout.write("mean:"+str(np.mean(ys))+"\n")
		fout.write("max:"+str(max(ys))+"\n")
		fout.write("min:"+str(min(ys))+"\n")
		fout.write("\n")
		fout.write("alto,ancho,cantidad\n")
		for i in xrange(len(z)):
			fout.write( str(xs[i])+","+str(ys[i])+","+str(z[i])+"\n" )


	# cv2.namedWindow('image', cv2.WINDOW_NORMAL)
	# cv2.imshow('image',img)
	# cv2.waitKey(0)
	# cv2.destroyAllWindows()