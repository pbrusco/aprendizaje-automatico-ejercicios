# usr/bin/python
# -*- coding: utf-8 -*-

import sys
import numpy as np
import cv2
import glob
import config
import matplotlib.pyplot as plt

# dimensionalidad
from sklearn.decomposition import PCA

# clasificadores
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.svm import LinearSVC

# ensambles
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import AdaBoostClassifier
from VotingClassifier import VotingClassifier # from file

# tuneo
from sklearn.grid_search import GridSearchCV

# métricas y validación
from sklearn.cross_validation import train_test_split
from sklearn.cross_validation import cross_val_score
from sklearn.cross_validation import StratifiedKFold
import sklearn.metrics as metrics
from time import time

# feature selection
from sklearn.feature_selection import VarianceThreshold

# image feature extraction
from sklearn.feature_extraction import image

# preprocessing
from sklearn.preprocessing import Binarizer
from sklearn.preprocessing import PolynomialFeatures

# computer vision library
import mahotas

# Variables globales
MAX_IMGS = 24998
# MAX_IMGS = 1000 # for testing purposes

# for patches based approach
SMALL_PATTERN_SIZE = 3
PATCHES_IMG_RESIZE = 4*20

def extract_tag(img_filename):
  return "dog" in img_filename

class Problem:
  def __init__(self):
    self.img_filenames = []
    self.img_tags = []

  def read_problem(self):
    images_folder = config.dataset_folder
    self.img_filenames = glob.glob(images_folder + "/*.jpg")
    self.img_tags = np.array([extract_tag(filename) for filename in self.img_filenames])
    return (self.img_filenames, self.img_tags)
# usage():
#   print
#   print "    usage: "
#   print "        python tp2.py    dataset_folder"
#   print
#   print "    example: "
#   print "        python tp2.py    datasets/train"
#   print

def plot_bars(y_dev_full, y_dev, y_val):
    n_groups = 3
    cats = (sum(y_dev_full), sum(y_dev), sum(y_val))
    dogs = (sum(1 - y_dev_full), sum(1 - y_dev), sum(1- y_val))

    fig, ax = plt.subplots()

    index = np.arange(n_groups)
    bar_width = 0.35

    opacity = 0.4
    error_config = {'ecolor': '0.3'}

    rects1 = plt.bar(index, cats, bar_width,
                     alpha=opacity,
                     color='b',
                     label='Gatos')

    rects2 = plt.bar(index + bar_width, dogs, bar_width,
                     alpha=opacity,
                     color='r',
                     label='Perros')

    plt.xlabel('Grupo')
    plt.ylabel('Numero de instancias')
    plt.title('Numero de instancias por set')
    plt.xticks(index + bar_width, ('Dev Full', 'Dev Fast', 'Validation'))
    plt.legend()
    return plt

def show(img):
  cv2.namedWindow('image', cv2.WINDOW_NORMAL)
  cv2.imshow('image',img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

def read_pixels(filenames):
  print "Reading images pixels"
  heigth = width = 75
  img_pixels = []
  for i, filename in enumerate(filenames):
    img = cv2.imread(filename,0)
    img = cv2.resize(img,(heigth,width))
    img_pixels.append( img.reshape(heigth*width) )
    if i%1000 == 0:
        print i, "de", len(filenames)

  return np.array(img_pixels)

def solve_by_pixels(prob):
  print "solving by pixels..."
  X = read_pixels(prob)
  y = prob.img_tags[0:len(X)]

  pca = PCA(n_components=50)

  skf = StratifiedKFold(y, n_folds=3, random_state=0)
  scores = []

  for train_index, test_index in skf:
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    aux = sum(y_train)
    print "#dogs:", aux
    print "#cats:", len(y_train) - aux

    # reducción de dimensionalidad
    print "applying PCA..."
    X_train_reduced = pca.fit_transform(X_train, y_train)
    X_test_reduced = pca.transform(X_test)
    print "done."
    print "% of variance captured:", np.sum(pca.explained_variance_ratio_)

    # clf = KNeighborsClassifier(n_neighbors=3, weights='distance')
    clf = RandomForestClassifier(n_estimators=250, random_state=0, n_jobs=-1)

    clf.fit(X_train_reduced,y_train)
    y_pred = clf.predict(X_test_reduced)

    scores.append(metrics.accuracy_score(y_test, y_pred))
    print "partition: #", len(scores), ", accuracy_score:", scores[-1]

  # The mean score and the 95% confidence interval of the score estimate are hence given by:
  scores = np.array(scores)
  print "mean accuracy: %0.2f (std: %0.2f)" % (scores.mean(), scores.std())

def read_histograms(filenames):
  img_histograms = []
  for filename in filenames:
    src_img = cv2.imread(filename)
    hs = []
    for c in xrange(3):
      img = src_img[:,:,c]
      hist, bin_edges = np.histogram(img.flatten(),256,[0,256])
      hs = hs + hist.tolist()
    img_histograms.append(hs)
  return np.array(img_histograms)

def solve_by_histogram(prob):
  print "solving by histogram..."
  X = read_histograms(prob)
  y = prob.img_tags[0:len(X)]

  # feature selection
  # TODO: experimentar con otros (comentado, porque con este cambiaba nada)
  # sel = VarianceThreshold(threshold=0.2)
  # X = sel.fit_transform(X)

  # clf1 = RandomForestClassifier(n_estimators=100, random_state=0)
  # clf2 = ExtraTreesClassifier(n_estimators=100, random_state=0)
  # clf3 = DecisionTreeClassifier(random_state=0)
  # clf4 = GradientBoostingClassifier(random_state=0)
  # clf5 = KNeighborsClassifier()

  # clfs = [('rf',clf1),('et',clf2),('dt',clf3),('gb',clf4),('knn',clf5)]
  # clfs = [('rf',clf1),('et',clf2),('dt',clf3),('gb',clf4),('knn',clf5)]
  # clf = VotingClassifier(estimators=clfs, voting='soft')
  clf = RandomForestClassifier(n_estimators=100, random_state=0)

  params = {'knn__n_neighbors': [1,3,5,10]}

  skf = StratifiedKFold(y, n_folds=5, random_state=0)
  scores = []

  for train_index, test_index in skf:
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    # grid = GridSearchCV(estimator=clf, param_grid=params, cv=5)
    # grid.fit(X_train,y_train)

    clf.fit(X_train,y_train)
    y_pred = clf.predict(X_test)

    scores.append(metrics.accuracy_score(y_test, y_pred))
    print "partition: #", len(scores), ", accuracy_score:", scores[-1]

  # The mean score and the 95% confidence interval of the score estimate are hence given by:
  scores = np.array(scores)
  print "mean accuracy: %0.2f (std: %0.2f)" % (scores.mean(), scores.std())

def white():
  return 255

def black():
  return 0

def initialize_patches():
  pathes_count = {}
  pattern_area = SMALL_PATTERN_SIZE*SMALL_PATTERN_SIZE
  ss = "{0:0" + str(pattern_area) + "b}"
  for pattern in xrange(2**pattern_area):
    b = ss.format(pattern)
    pathes_count[b] = 0
  return pathes_count

def extract_patches_count(filenames):
  # heigth = width = (4*15)
  heigth = width = PATCHES_IMG_RESIZE

  pc = initialize_patches()

  all_patches = []

  for filename in filenames:
    # for k in pc.keys():
    #     pc[k] = 0

    # transform_to_black_and_white
    src_img = cv2.imread(filename)
    src_img = cv2.resize(src_img,(heigth,width))

    vs = []

    for c in xrange(3):
      for k in pc.keys():
        pc[k] = 0

      img = src_img[:,:,c]

      bina = Binarizer(threshold=255/2.0)
      img = bina.fit_transform(img)

      # extraer patches
      img_patches = image.extract_patches_2d(img, (SMALL_PATTERN_SIZE, SMALL_PATTERN_SIZE))
      for p in img_patches:
        p = p.flatten().tolist()
        p = ''.join(str(e) for e in p)
        pc[p] += 1

      vs = vs + pc.values()

    # all_patches.append( vs+[acum_light] )
    all_patches.append( vs )

      # debug:
      # print pc.values()

  print "all patches extracted."
  return np.array(all_patches)

def solve_by_patches(prob):
  print
  print "***********************************************"
  print "solving by patches"

  print "extracting patches..."
  X = extract_patches_count(prob)
  # poly = PolynomialFeatures(2)
  # X = poly.fit_transform(X)
  print "done."
  y = prob.img_tags[0:len(X)]

  skf = StratifiedKFold(y, n_folds=5, random_state=0)
  scores = []

  # parameters = {'gb__learning_rate':[0.1,0.3,0.5]}

  for train_index, test_index in skf:
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    clf1 = RandomForestClassifier(n_estimators=100, random_state=0, n_jobs=-1)
    clf2 = GradientBoostingClassifier(n_estimators=100, random_state=0)
    clf3 = AdaBoostClassifier(n_estimators=100, random_state=0)

    clfs = [('rf',clf1),('gb',clf2),('ab',clf3)]
    clf = VotingClassifier(estimators=clfs, voting='soft')
    # clf = GridSearchCV(clf, parameters)

    print "fitting clf..."
    clf.fit(X_train,y_train)
    print "done."
    y_pred = clf.predict(X_test)

    scores.append(metrics.accuracy_score(y_test, y_pred))
    print "partition: #", len(scores), ", accuracy_score:", scores[-1]

  # The mean score and the 95% confidence interval of the score estimate are hence given by:
  scores = np.array(scores)
  print "mean accuracy: %0.2f (std: %0.2f)" % (scores.mean(), scores.std())

def extract_pixels_connections(filenames):
  heigth, width = 75, 75
  images = []
  for filename in filenames:
    img = cv2.imread(filename,0)
    img = cv2.resize(img,(heigth,width))
    g = image.img_to_graph(img)
    g = g.todok()

    images.append( g.values() )

  return np.array(images)

def solve_by_pixels_connections(prob):
  print
  print "***********************************************"
  print "solving by pixels connections"

  print "extracting pixels connections..."
  X = extract_pixels_connections(prob)
  y = prob.img_tags[0:len(X)]
  print "done."

  print "X.shape:", X.shape

  skf = StratifiedKFold(y, n_folds=5, random_state=0)
  scores = []

  for train_index, test_index in skf:
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    clf = RandomForestClassifier(n_estimators=100, random_state=0)

    print "fitting clf..."
    clf.fit(X_train,y_train)
    print "done."
    y_pred = clf.predict(X_test)

    scores.append(metrics.accuracy_score(y_test, y_pred))
    print "partition: #", len(scores), ", accuracy_score:", scores[-1]

  # The mean score and the 95% confidence interval of the score estimate are hence given by:
  scores = np.array(scores)
  print "mean accuracy: %0.2f (std: %0.2f)" % (scores.mean(), scores.std())

def extract_test_set(images_folder):
  img_filenames = glob.glob(images_folder + "/*.jpg")

  img_filenames = sorted(img_filenames, key= lambda name: int(name[name.rindex('/')+1:name.index('.')]))

  heigth = width = PATCHES_IMG_RESIZE
  pc = initialize_patches()
  all_patches = []

  for filename in img_filenames:
    print len(all_patches)+1, filename

    # transform_to_black_and_white
    src_img = cv2.imread(filename)
    src_img = cv2.resize(src_img,(heigth,width))

    vs = []

    for c in xrange(3):
      for k in pc.keys():
        pc[k] = 0
      img = src_img[:,:,c]

      for i in xrange(heigth):
        for j in xrange(width):
          px = img[i][j]
          if px > (white()/2.0):
            img[i][j] = 1
          else:
            img[i][j] = 0
      # extraer patches
      img_patches = image.extract_patches_2d(img, (SMALL_PATTERN_SIZE, SMALL_PATTERN_SIZE))
      for p in img_patches:
        p = p.flatten().tolist()
        p = ''.join(str(e) for e in p)
        pc[p] += 1
      vs = vs + pc.values()
    all_patches.append( vs )
  return np.array(all_patches)

def solve_for_submission(prob):
  print "***********************************************"
  print "solving for submission"

  test_folder = sys.argv[2]
  X_test = extract_test_set(test_folder)

  print "extracting patches..."
  X_train = extract_patches_count(prob)
  print "done."
  y_train = prob.img_tags[0:len(X_train)]

  clf = RandomForestClassifier(n_estimators=500, random_state=0, n_jobs=-1)

  print "fitting clf..."
  clf.fit(X_train,y_train)
  print "done."
  y_pred = clf.predict(X_test)

  header = "id,label\n"
  solution_filename = 'solution.csv'
  with open(solution_filename) as fout:
    fout.write(header)
    print header
    for i in xrange(len(y_pred)):
      row = str(i+1)+","+str(int(y_pred[i]))+"\n"
      fout.write(row)
      print row

  print "Solution file generated:", solution_filename+"."

def extract_lbp_count(filenames):
  heigth = width = PATCHES_IMG_RESIZE

  all_patches = []

  radius = 1.0
  points = 8

  for filename in filenames:
    src_img = cv2.imread(filename)
    src_img = cv2.resize(src_img,(heigth,width))

    vs = []

    for c in xrange(3):
      img = src_img[:,:,c]

      mh = mahotas.features.lbp(img, radius, points, ignore_zeros=False)
      vs = vs + mh.tolist()

    all_patches.append( vs )

  print "all images processed."
  return np.array(all_patches)

def solve_by_lbp(prob):
  print
  print "***********************************************"
  print "solving by lbp"

  print "extracting lbp..."
  X = extract_lbp_count(prob)
  print "done."
  print "X.shape:", X.shape
  y = prob.img_tags[0:len(X)]

  skf = StratifiedKFold(y, n_folds=5, random_state=0)
  scores = []

  for train_index, test_index in skf:
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    clf1 = RandomForestClassifier(n_estimators=100, random_state=0, n_jobs=-1)
    clf2 = GradientBoostingClassifier(n_estimators=100, random_state=0)
    clf3 = AdaBoostClassifier(n_estimators=100, random_state=0)

    clfs = [('rf',clf1),('gb',clf2),('ab',clf3)]
    clf = VotingClassifier(estimators=clfs, voting='soft')

    print "fitting clf..."
    clf.fit(X_train,y_train)
    print "done."
    y_pred = clf.predict(X_test)

    scores.append(metrics.accuracy_score(y_test, y_pred))
    print "partition: #", len(scores), ", accuracy_score:", scores[-1]

  # The mean score and the 95% confidence interval of the score estimate are hence given by:
  scores = np.array(scores)
  print "mean accuracy: %0.2f (std: %0.2f)" % (scores.mean(), scores.std())

def solve(prob):

  solve_by_pixels(prob)
  # solve_by_histogram(prob)
  # solve_by_patches(prob)
  # solve_by_pixels_connections(prob)

  # solve_by_lbp(prob)

  # solve_for_submission(prob)

# if __name__ == '__main__':
#
#     sys.exit()
#
#   t = time()
#
#   prob = Problem()
#   prob.read_problem(sys.argv[1])
#
#   solve(prob)
#
#   print str(round(time() - t, 2))+"secs."
def accuracy(conf_matrix):
    return (conf_matrix[0,0] + conf_matrix[1,1]) / float(sum(sum(conf_matrix[:])))

def recall(conf_matrix, classs):
    return conf_matrix[classs,classs] / float(sum(conf_matrix[classs,:]))

class ConfusionTable(object):
    def __init__(self, conf_matrix):
        self.m = conf_matrix

    def html(self):
        return "<table>\
            <tr><td></td><th>Gatos (predicted)</th><th>Perros (predicted)</th></tr> \
            <tr><th>Gatos (actual)</th><td>{}</td><td>{}</td></tr> \
            <tr><th>Perros (actual)</th><td>{}</td><td>{}</td></tr> \
            <tr bgcolor='#F0F0FF '><th>Accuracy</th><td colspan='2'>{}</td></tr> \
            </table>".format(self.m[0,0], self.m[0,1], self.m[1,0], self.m[1,1], accuracy(self.m))

    def _repr_html_(self):
        return self.html()

class DictTable(dict):
    # Overridden dict class which takes a dict in the form {'a': 2, 'b': 3},
    # and renders an HTML Table in IPython Notebook.
    def draw(self):
        html = ["<table>"]
        for key, value in self.iteritems():
            html.append("<tr>")
            html.append("<td>{0}</td>".format(key))
            if type(value) == dict:
                html.append("<td>" + DictTable(value).draw() + "</td>")
            else:
                table = ConfusionTable(value)
                html.append("<td>{}</td>".format(table.html()))
            html.append("</tr>")
        html.append("</table>")
        return ''.join(html)

    def _repr_html_(self):
        return self.draw()
