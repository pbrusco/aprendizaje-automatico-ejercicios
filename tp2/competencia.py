# usr/bin/python
# -*- coding: utf-8 -*-
import tp2
import cv2
import sklearn
import glob
import numpy as np
import pickle
import sys
from sklearn.externals import joblib


competition_images_folder = sys.argv[1]

competition_filenames = glob.glob(competition_images_folder + "/*.jpg")
competition_filenames = sorted(competition_filenames, key= lambda name: int(name[name.rindex('/')+1:name.index('.')]))

clf = joblib.load('modelo_back/best-model.pkl') # Para cargar el modelo
X_val_features = tp2.extract_patches_count(competition_filenames)
y_pred = clf.predict(X_val_features)
pickle.dump( y_pred, open( "resultado-final-competicion.p", "wb" ) )


# Script para generar el archivo solución para la competencia
header = "id,label\n"
solution_filename = 'solution-competencia.csv'
with open(solution_filename,'w') as fout:
    fout.write(header)
    for i, filename in enumerate(competition_filenames):
        row = filename + "," + str(int(y_pred[i]))+"\n"
        fout.write(row)
