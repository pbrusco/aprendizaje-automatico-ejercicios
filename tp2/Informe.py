# coding: utf-8
import tp2
import sys

import sklearn
import numpy as np
import pickle

problem = tp2.Problem()
X, y = problem.read_problem()
y = y*1

print "Full dataset instances", len(X)

print "# dogs:", sum(problem.img_tags)
print "# cats:", len(problem.img_tags) - sum(problem.img_tags)

# Dev-Test sets.
X_dev_full, X_val, y_dev_full, y_val = sklearn.cross_validation.train_test_split(X, y, test_size=0.20, random_state=123)
dev_size = int(sys.argv[1])

X_dev = X_dev_full[0:dev_size]
y_dev = np.array(y_dev_full[0:dev_size])

print "Develompent size (fast): ", len(X_dev)
print "Develompent size (full): ", len(X_dev_full)
print "Validation size: ", len(X_val)

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import AdaBoostClassifier
from VotingClassifier import VotingClassifier # from file

classifiers = []
results = {}

clf1 = RandomForestClassifier(n_estimators=100, random_state=0)
classifiers.append(("Random Forest Classifier", clf1))

clf2 = ExtraTreesClassifier(n_estimators=100, random_state=0)
classifiers.append(("Extra Trees Classifier", clf2))

clf3 = GradientBoostingClassifier(random_state=0)
classifiers.append(("Gradient Boosting Classifier", clf3))

clf4 = AdaBoostClassifier(n_estimators=100, random_state=0)
classifiers.append(("Ada Boost Classifier", clf4))

clf5 = VotingClassifier(estimators=[('rf', clf1), ('et', clf2), ('gb', clf3), ('ada', clf4)], voting='soft')
classifiers.append(("Voting Classifier", clf5))

from sklearn.metrics import confusion_matrix

def classifiers_scores(X_train, y_train, X_test, y_test):
    classifiers_results = {}
    for name, clf in classifiers:
        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        classifiers_results[name] = confusion_matrix(y_test, y_pred, labels=[0,1])
    return classifiers_results


skf = sklearn.cross_validation.StratifiedKFold(y_dev, n_folds=2, random_state=0)

def test_algorithm(extraction_algorithm, algorithm_name):
    X_features = extraction_algorithm(X_dev)
    results[algorithm_name] = {}

    for i, (train_index, test_index) in enumerate(skf):
        print "Fold", i+1
        X_train, X_test = X_features[train_index], X_features[test_index]
        y_train, y_test = y_dev[train_index], y_dev[test_index]

        classifiers_results = classifiers_scores(X_train, y_train, X_test, y_test)
        results[algorithm_name][i+1] = classifiers_results


from sklearn.decomposition import PCA

X_dev_pixels = tp2.read_pixels(X_dev)

pca = PCA(n_components=50)
results["PCA"] = {}

for i, (train_index, test_index) in enumerate(skf):
    print "Fold", i+1
    X_train, X_test = X_dev_pixels[train_index], X_dev_pixels[test_index]
    y_train, y_test = y_dev[train_index], y_dev[test_index]

    X_train_reduced = pca.fit_transform(X_train, y_train)
#     print "PCA: % of variance captured:", np.sum(pca.explained_variance_ratio_)
    X_test_reduced = pca.transform(X_test)

    classifiers_results = classifiers_scores(X_train_reduced, y_train, X_test_reduced, y_test)
    results["PCA"][i+1] = classifiers_results


test_algorithm(tp2.read_histograms, "Histogramas")
test_algorithm(tp2.extract_patches_count, "Patches")
test_algorithm(tp2.extract_pixels_connections, "Pixel Connections")
test_algorithm(tp2.extract_lbp_count, "LBP Count")

pickle.dump( results, open( "results-size-{}.p".format(len(y_dev)), "wb" ) )
