import csv
from sklearn import tree
import numpy as np

X = []
Y = []

with open('data.csv', 'rb') as csvfile:
    spamreader = csv.reader(csvfile)

    for index, row in enumerate(spamreader):
        if index > 0:
            X.append(row[3:])
            Y.append(row[2])
        else:
            labels = row[3:]

clf = tree.DecisionTreeClassifier()
clf = clf.fit(X, Y)
print clf

from sklearn.externals.six import StringIO
with open("quien.dot", 'w') as f:
     f = tree.export_graphviz(clf, out_file=f, feature_names=labels)


X_test = []
with open('test.csv', 'rb') as csvfile:
    spamreader = csv.reader(csvfile)
    for row in spamreader:
        X_test.append(row[3:])

print clf.predict(X_test)
