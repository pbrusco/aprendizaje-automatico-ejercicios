function [J, grad] = costFunctionReg(theta, X, y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters.
lambda = 0;
% Initialize some useful values
m = length(y); % number of training examples
n = length(theta);

% You need to return the following variables correctly
cost_summatory = 0;
for i=1:m
  h = sigmoid(theta' * X(i,:)');
  fst_term = - y(i) * log(h);
  snd_term = (1 - y(i)) * log(1 - h);

  cost_summatory += fst_term - snd_term;
endfor

reg_factor = sum([0; theta(2:n)] .^ 2);

J = (1/m) * cost_summatory + (lambda/(2*m))*reg_factor;

gradient_summatory = zeros(n, 1);
for i=1:m
  h = sigmoid(theta' * X(i,:)');
  gradient_summatory += (h - y(i)) * X(i,:)';
endfor

gradient_summatory_first_term = (1/m) .* gradient_summatory;
gradient_reg_factor = (lambda/m).*theta;
grad =  gradient_summatory_first_term .+ [0; gradient_reg_factor(2:n)];




% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta






% =============================================================

end
