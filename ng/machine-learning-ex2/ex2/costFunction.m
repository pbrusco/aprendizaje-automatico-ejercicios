function [J, grad] = costFunction(theta, X, y)
%COSTFUNCTION Compute cost and gradient for logistic regression
%   J = COSTFUNCTION(theta, X, y) computes the cost of using theta as the
%   parameter for logistic regression and the gradient of the cost
%   w.r.t. to the parameters.

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly
cost_summatory = 0;
for i=1:m
  h = sigmoid(theta' * X(i,:)');
  fst_term = - y(i) * log(h);
  snd_term = (1 - y(i)) * log(1 - h);

  cost_summatory += fst_term - snd_term;
endfor

J = (1/m) * cost_summatory ;

gradient_summatory = 0;
for i=1:m
  h = sigmoid(theta' * X(i,:)');
  gradient_summatory += (h - y(i)) * X(i,:);
endfor

grad = (1/m) * gradient_summatory;

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Note: grad should have the same dimensions as theta
%








% =============================================================

end
