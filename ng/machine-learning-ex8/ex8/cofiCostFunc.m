function [J, grad] = cofiCostFunc(params, Y, R, num_users, num_movies, ...
                                  num_features, lambda)
%COFICOSTFUNC Collaborative filtering cost function
%   [J, grad] = COFICOSTFUNC(params, Y, R, num_users, num_movies, ...
%   num_features, lambda) returns the cost and gradient for the
%   collaborative filtering problem.
%

% Unfold the U and W matrices from params
X = reshape(params(1:num_movies*num_features), num_movies, num_features);
Theta = reshape(params(num_movies*num_features+1:end), ...
                num_users, num_features);


% You need to return the following values correctly
J = 0;
X_grad = zeros(size(X));
Theta_grad = zeros(size(Theta));

term = (X * Theta' - Y);

J = (1/2) * sum(sum(((term .^ 2) .* R)));
J = J + lambda/ 2 * sum(sum(Theta.^2)) + lambda / 2 * sum(sum(X.^2));

for j = 1: num_users
    indices = find(R(:, j) == 1);
    T = Theta(j, :);
    Y2 = Y(indices, j);
    X2 = X(indices, :);
    Theta_grad(j,:) = (X2 * T' - Y2)' * X2 + lambda * Theta(j, :);
% =============================================================
end

for i = 1 : num_movies
    indices = find(R(i, :) == 1);
    Theta_temp = Theta(indices, :);
    Y_tem = Y(i, indices);
    X_grad(i, :) = (X(i, :) * Theta_temp' - Y_tem) * Theta_temp + lambda * X(i,:);
end

grad = [X_grad(:); Theta_grad(:)];

end
